<?php

namespace HypeDevGroup\JWTRedis\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use HypeDevGroup\JWTRedis\Facades\RedisCache;

class ProcessObserver implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    private Model $model;

    private string $process;


    public function __construct(Model $model, string $process)
    {
        $this->afterCommit = true;
        $this->model = $model;
        $this->process = $process;
    }

    public function handle(): void
    {
        $method = $this->process;

        $this->$method();
    }

    public function deleted(): bool
    {
        return RedisCache::key($this->model->getRedisKey())->removeCache();
    }

    public function updated(): mixed
    {
        return RedisCache::key($this->model->getRedisKey())
            ->data($this->model->load(config('jwtredis.cache_relations')))
            ->refreshCache();
    }
}
