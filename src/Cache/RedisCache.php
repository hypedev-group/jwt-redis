<?php

namespace HypeDevGroup\JWTRedis\Cache;

use Illuminate\Support\Facades\Redis;
use HypeDevGroup\JWTRedis\Contracts\RedisCacheContract;

class RedisCache implements RedisCacheContract
{
    protected mixed $data;

    private int $time;

    protected string $key;

    public function key(string $key): RedisCacheContract
    {
        $this->key = $key;

        return $this;
    }

    public function data($data): RedisCacheContract
    {
        $this->data = $data;

        return $this;
    }

    public function getCache(): mixed
    {
        $data = Redis::connection(config('jwtredis.redis_connection'))->get($this->key);

        if (!is_null($data)) {
            return $this->unserialize($data);
        }

        return null;
    }

    public function removeCache(): bool
    {
        return Redis::connection(config('jwtredis.redis_connection'))->del($this->key);
    }

    public function refreshCache(): mixed
    {
        $this->key($this->key)->removeCache();

        return $this->key($this->key)->data($this->data)->cache();
    }

    public function cache(): mixed
    {
        $this->setTime();

        Redis::connection(config('jwtredis.redis_connection'))->set($this->key, $this->serialize($this->data));

        return $this->data;
    }

    private function setTime(): void
    {
        $this->time = (config('jwtredis.redis_ttl_jwt') ? config('jwt.ttl') : config('jwtredis.redis_ttl')) * 60;
    }

    protected function serialize($value): int|string
    {
        if (config('jwtredis.igbinary_serialization')) {
            return igbinary_serialize($value);
        }

        return serialize($value);
    }

    protected function unserialize($value): mixed
    {
        if (config('jwtredis.igbinary_serialization')) {
            return igbinary_unserialize($value);
        }

        return unserialize($value, ['allowed_classes' => true]);
    }
}
