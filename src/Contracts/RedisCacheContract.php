<?php

namespace HypeDevGroup\JWTRedis\Contracts;

interface RedisCacheContract
{
    public function key(string $key): RedisCacheContract;

    public function data($data): RedisCacheContract;

    public function removeCache(): bool;

    public function getCache(): mixed;


    public function refreshCache(): mixed;

    public function cache(): mixed;
}
