<?php

namespace HypeDevGroup\JWTRedis\Traits;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles as OriginalHasRole;

trait JWTRedisHasRoles
{
    use OriginalHasRole {
        assignRole as protected originalAssignRole;
        givePermissionTo as protected originalGivePermissionTo;
    }

    public function assignRole(...$roles): self
    {
        $this->originalAssignRole(...$roles);

        $this->triggerTheObserver();

        return $this;
    }

    public function givePermissionTo(...$permissions): self
    {
        $this->originalGivePermissionTo(...$permissions);

        $this->triggerTheObserver();

        return $this;
    }

    public function hasPermissionTo($permission, $guardName = null): bool
    {
        return $this->hasDirectPermission($permission) || $this->hasPermissionViaRole($permission);
    }

    public function hasDirectPermission($permission): bool
    {
        if (is_string($permission)) {
            return $this->permissions->contains('name', $permission);
        }

        if (is_int($permission)) {
            return $this->permissions->contains('id', $permission);
        }
    }

    public function hasPermissionViaRole($permission): bool
    {
        $roles = $this->roles;

        if (is_string($permission)) {
            foreach ($roles as $role) {
                if ($role->permissions->contains('name', $permission)) {
                    return true;
                }
            }
        }

        if (is_int($permission)) {
            foreach ($roles as $role) {
                if ($role->permissions->contains('id', $permission)) {
                    return true;
                }
            }
        }

        return false;
    }
    
    public function checkUserStatus(): bool
    {
        $column = config('jwtredis.status_column_title');
        $values = config('jwtredis.banned_statuses');

        return !in_array($this->$column, $values);
    }

    public function getRedisKey(): string
    {
        return config('jwtredis.redis_auth_prefix') . $this->getJWTIdentifier();
    }

    public function triggerTheObserver(): void
    {
        /** @var Model $model */
        $model = $this;

        $class = config('jwtredis.observer');

        (new $class())->updated($model);
    }
}
