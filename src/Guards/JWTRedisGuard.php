<?php

namespace HypeDevGroup\JWTRedis\Guards;

use Illuminate\Contracts\Auth\Authenticatable;
use HypeDevGroup\JWTRedis\Facades\RedisCache;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\JWTGuard;

class JWTRedisGuard extends JWTGuard
{
    public function once(array $credentials = []): bool
    {
        if ($this->validate($credentials)) {
            $this->setUser($this->lastAttempted);

            $this->storeRedis(true);

            return true;
        }

        return false;
    }

    public function user(): ?Authenticatable
    {
        if (!$this->user && $this->jwt->getToken()){
            $this->user = $this->retrieveByRedis();
        }

        return $this->user ?? null;
    }

    public function attempt(array $credentials = [], $login = true): bool|string
    {
        $this->lastAttempted = $user = $this->provider->retrieveByCredentials($credentials);

        if ($this->hasValidCredentials($user, $credentials)) {
            $this->refreshAuthFromRedis($user);

            return $login ? $this->login($user) : true;
        }

        return false;
    }

    public function retrieveByRedis()
    {
        return $this->request->authedUser ?? $this->getOrSetToRedis();
    }

    public function getOrSetToRedis()
    {
        return $this->getAuthFromRedis() ?? $this->setAuthToRedis();
    }

    public function getAuthFromRedis()
    {
        return RedisCache::key($this->getRedisKeyFromClaim())->getCache();
    }

    public function refreshAuthFromRedis($user)
    {
        return RedisCache::key($user->getRedisKey())->data($user)->refreshCache();
    }

    public function removeAuthFromRedis(): bool
    {
        return RedisCache::key($this->getRedisKeyFromClaim())->removeCache();
    }

    public function getRedisKeyFromClaim(): string
    {
        $payload = JWTAuth::parseToken()->getPayload()->toArray();

        return 'auth_' . $payload['sub'];
    }

    public function setAuthToRedis(): mixed
    {
        if ($this->request->bearerToken()) {
            return $this->storeRedis();
        }

        return null;
    }

    public function storeRedis(bool $login = false): mixed
    {
        if (!$login) {
            return RedisCache::key($this->getRedisKeyFromClaim())
                ->data(JWTAuth::parseToken()->authenticate()->load(config('jwtredis.cache_relations')))
                ->cache();
        }

        return RedisCache::key($this->lastAttempted->getRedisKey())->data($this->lastAttempted)->cache();
    }
}
