<?php

namespace HypeDevGroup\JWTRedis;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use HypeDevGroup\JWTRedis\Cache\RedisCache;
use HypeDevGroup\JWTRedis\Contracts\RedisCacheContract;
use HypeDevGroup\JWTRedis\Guards\JWTRedisGuard;
use HypeDevGroup\JWTRedis\Providers\JWTRedisUserProvider;

class JWTRedisServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->bindRedisCache();
    }

    public function boot(): void
    {
        $this->publishConfig();
        $this->overrideJWTGuard();
        $this->overrideUserProvider();
        $this->bindObservers();
    }

    protected function publishConfig(): void
    {
        $this->mergeConfigFrom(__DIR__.'/config/jwtredis.php', 'jwtredis');

        $this->publishes([__DIR__.'/config/jwtredis.php' => config_path('jwtredis.php')], 'config');
    }

    protected function overrideJWTGuard(): void
    {
        Auth::extend('jwt_redis_guard', static function ($app, $name, array $config) {
            return new JWTRedisGuard($app['tymon.jwt'], Auth::createUserProvider($config['provider']), $app['request']);
        });
    }

    protected function overrideUserProvider(): void
    {
        Auth::provider('jwt_redis_user_provider', static function ($app, array $config) {
            return new JWTRedisUserProvider($app['hash'], $config['model']);
        });
    }

    protected function bindRedisCache(): void
    {
        $this->app->bind(RedisCacheContract::class, function ($app) {
            return new RedisCache();
        });
    }

    protected function bindObservers(): void
    {
        if (class_exists(config('jwtredis.user_model')) && config('jwtredis.is_provider')) {
            config('jwtredis.user_model')::observe(config('jwtredis.observer'));
        }
    }
}
