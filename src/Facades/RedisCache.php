<?php

namespace HypeDevGroup\JWTRedis\Facades;

use Illuminate\Support\Facades\Facade;
use HypeDevGroup\JWTRedis\Contracts\RedisCacheContract;

/**
 * Class RedisCache.
 * @method static RedisCacheContract key($getRedisKey)
 */
class RedisCache extends Facade
{
    public static function getFacadeAccessor(): string
    {
        return RedisCacheContract::class;
    }
}
