<?php

namespace HypeDevGroup\JWTRedis\Observers;

use Illuminate\Database\Eloquent\Model;
use HypeDevGroup\JWTRedis\Jobs\ProcessObserver;

class UserRedisObserver
{

    public function updated(Model $model): void
    {
        $this->handler($model, __FUNCTION__);
    }

    public function deleted(Model $model): void
    {
        $this->handler($model, __FUNCTION__);
    }

    protected function handler(Model $model, string $action): void
    {
        $handler = new ProcessObserver($model, $action);

        config('jwtredis.observer_events_queue') ? dispatch($handler) : $handler->updated();
    }
}
